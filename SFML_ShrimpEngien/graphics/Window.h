#pragma once

#include <string>
#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Camera.h"

	class ShrimpWindow {
	public:
		sf::RenderWindow* m_Window;
		int m_Width;
		int m_Height;
		std::string m_Title;
		sf::Vector2i m_MousePos;
		sf::Clock c;
		int m_Type;
		bool m_Focus;
		Player* m_Player;
		Camera* m_Cam;
	public:
		const static int WINDOW_GL_2D = 1;
		const static int WINDOW_GL_3D = 2;
		ShrimpWindow(int width, int height, std::string title, int type);
		void update();
		void clear();
		void poll();
		void setFPSLimit(int fps);
		bool isCloseRequested();
		//sf::Vector2i mousePos;
		sf::Vector2i getSize();

		int displayLoadError(std::string text) {
			std::cout << "Failed to load " << text << "!!" << std::endl;
			getchar();
			return 1;
		}

		sf::RenderWindow* getWindow() {
			return m_Window;
		}

		sf::Vector2i getMousePos() {
			return m_MousePos;
		}

		bool isMousePressed() {
			return sf::Mouse::isButtonPressed(sf::Mouse::Left);
		}

		void showMouse(bool show) {
			m_Window->setMouseCursorVisible(show);
		}

		bool getFocus() {
			return m_Focus;
		}

		Player* getPlayer() {
			return m_Player;
		}
	};

#include "Quad3D.h"

Quad3D::Quad3D(sf::Vector3f v[], sf::Texture* tex) {
	
	m_Vects[0] = v[0];
	m_Vects[1] = v[1];
	m_Vects[2] = v[2];
	m_Vects[3] = v[3];

	m_Tex = tex;
}

void Quad3D::render() {
	glPushMatrix();
	//if (m_Tex != NULL) {
	//	sf::Texture::bind(m_Tex);
	//}

	glBegin(GL_QUADS);

	glTexCoord2f(0, 1);
	glVertex3f(m_Vects[0].x, m_Vects[0].y, m_Vects[0].z);
	glTexCoord2f(1, 1);
	
	glVertex3f(m_Vects[1].x, m_Vects[1].y, m_Vects[1].z);
	glTexCoord2f(1, 0);
	
	glVertex3f(m_Vects[2].x, m_Vects[2].y, m_Vects[2].z);
	glTexCoord2f(0, 0);
	
	glVertex3f(m_Vects[3].x, m_Vects[3].y, m_Vects[3].z);

	glEnd();

	//if (m_Tex != NULL) {
	//	sf::Texture::bind(NULL);
	//}
	glPopMatrix();
}
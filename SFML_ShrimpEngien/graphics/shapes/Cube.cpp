#include "Cube.h"

Cube::Cube(sf::Vector3f* t, sf::Vector3f* s, sf::Vector3f* r, sf::Texture* tx) {
	rot = r;
	pos = t;
	size = s;
	tex = tx;

	sf::Vector3f v0[4];
	sf::Vector3f v1[4];
	sf::Vector3f v2[4];
	sf::Vector3f v3[4];
	sf::Vector3f v4[4];
	sf::Vector3f v5[4];

	float x = s->x / 2;
	float y = s->y / 2;
	float z = s->z / 2;

	//back
	v0[1] = sf::Vector3f(-x, -y, -z);
	v0[0] = sf::Vector3f(x, -y, -z);
	v0[3] = sf::Vector3f(x, y, -z);
	v0[2] = sf::Vector3f(-x, y, -z);

	//front
	v1[0] = sf::Vector3f(-x, -y, z);
	v1[1] = sf::Vector3f(x, -y, z);
	v1[2] = sf::Vector3f(x, y, z);
	v1[3] = sf::Vector3f(-x, y, z);

	//left
	v2[0] = sf::Vector3f(-x, -y, -z);
	v2[1] = sf::Vector3f(-x, -y, z);
	v2[2] = sf::Vector3f(-x, y, z);
	v2[3] = sf::Vector3f(-x, y, -z);

	//right
	v3[1] = sf::Vector3f(x, -y, -z);
	v3[2] = sf::Vector3f(x, y, -z);
	v3[3] = sf::Vector3f(x, y, z);
	v3[0] = sf::Vector3f(x, -y, z);

	//top
	v4[3] = sf::Vector3f(-x, y, -z);
	v4[0] = sf::Vector3f(-x, y, z);
	v4[1] = sf::Vector3f(x, y, z);
	v4[2] = sf::Vector3f(x, y, -z);

	//bottom
	v5[2] = sf::Vector3f(x, -y, z);
	v5[3] = sf::Vector3f(-x, -y, z);
	v5[0] = sf::Vector3f(-x, -y, -z);
	v5[1] = sf::Vector3f(x, -y, -z);

	m_Quads[0] = new Quad3D(v0, tx);
	m_Quads[1] = new Quad3D(v1, tx);
	m_Quads[2] = new Quad3D(v2, tx);
	m_Quads[3] = new Quad3D(v3, tx);
	m_Quads[4] = new Quad3D(v4, tx);
	m_Quads[5] = new Quad3D(v5, tx);
}

void Cube::render() {
	glPushMatrix();

	glTranslatef(pos->x, pos->y, pos->z);

	glRotatef(rot->x, 1, 0, 0);
	glRotatef(rot->y, 0, 1, 0);
	glRotatef(rot->z, 0, 0, 1);

	sf::Texture::bind(tex);

	m_Quads[0]->render();
	m_Quads[1]->render();
	m_Quads[2]->render();
	m_Quads[3]->render();
	m_Quads[4]->render();
	m_Quads[5]->render();

	sf::Texture::bind(NULL);

	glPopMatrix();
}
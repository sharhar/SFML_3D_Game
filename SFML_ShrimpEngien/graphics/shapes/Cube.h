#pragma once

#include "Quad3D.h"

class Cube{
private:
	Quad3D* m_Quads[6];
	sf::Texture* tex;
public:
	Cube(sf::Vector3f* t, sf::Vector3f* s, sf::Vector3f* r, sf::Texture* tx);

	void render();
public:
	sf::Vector3f* pos;
	sf::Vector3f* rot;
	sf::Vector3f* size;
};
#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

class Quad3D {
private:
	sf::Vector3f m_Vects[4];
	sf::Texture* m_Tex;
public:
	Quad3D(sf::Vector3f v[], sf::Texture* tex);

	void render();
};
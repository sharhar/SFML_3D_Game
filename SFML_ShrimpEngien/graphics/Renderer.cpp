#include "Renderer.h"

void Renderer::renderRect2DTex(float x, float y, float w, float h, float r, const sf::Texture *tex) {
	glPushMatrix();

	glTranslatef(w/2 + x, h/2 + y, 0);
	glRotatef(r, 0, 0, 1);

	sf::Texture::bind(tex);

		glBegin(GL_QUADS);
			glTexCoord2f(0, 1);
			glVertex2f(-w/2, -h/2);
			glTexCoord2f(1, 1);
			glVertex2f(w / 2, -h / 2);
			glTexCoord2f(1, 0);
			glVertex2f(w / 2, h / 2);
			glTexCoord2f(0, 0);
			glVertex2f(-w / 2, h / 2);
		glEnd();

	sf::Texture::bind(NULL);

	glPopMatrix();
}

void Renderer::renderRect2D(float x, float y, float w, float h, float r) {
	glPushMatrix();

	glTranslatef(w / 2 + x, h / 2 + y, 0);
	glRotatef(r, 0, 0, 1);

	glBegin(GL_QUADS);
	glVertex2f(-w / 2, -h / 2);
	glVertex2f(w / 2, -h / 2);
	glVertex2f(w / 2, h / 2);
	glVertex2f(-w / 2, h / 2);
	glEnd();

	glPopMatrix();
}

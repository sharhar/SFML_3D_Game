#pragma once

#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>

class Renderer {
public:
	static void renderRect2D(float x, float y, float w, float h, float r);
	static void renderRect2DTex(float x, float y, float w, float h, float r, const sf::Texture *tex);
};
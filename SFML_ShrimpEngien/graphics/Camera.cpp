#include "Camera.h"

Camera::Camera(Player* p) {
	m_Player = p;
}

void Camera::useCam() {
	glRotatef(m_Player->getRot()->x, 1, 0, 0);
	glRotatef(m_Player->getRot()->y, 0, 1, 0);
	glRotatef(m_Player->getRot()->z, 0, 0, 1);

	glTranslatef(-m_Player->getPos()->x, -m_Player->getPos()->y, -m_Player->getPos()->z);
}
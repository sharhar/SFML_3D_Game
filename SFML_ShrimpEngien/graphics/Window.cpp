
#include "Window.h"

	ShrimpWindow::ShrimpWindow(int width, int height, std::string title, int type) {
		m_Width = width;
		m_Height = height;
		m_Title = title;
		m_Type = type;
		m_Focus = true;

		sf::ContextSettings context;
		context.depthBits = 32;

		m_Window = new sf::RenderWindow(sf::VideoMode(width, height), title, sf::Style::Default, context);

		m_Window->setActive();

		if (type == WINDOW_GL_2D) {
			glMatrixMode(GL_PROJECTION_MATRIX);
			glOrtho(0, 1600, 0, 900, -1, 1);
			glMatrixMode(GL_MODELVIEW_MATRIX);
		}
		else if (type == WINDOW_GL_3D) {
			glMatrixMode(GL_PROJECTION_MATRIX);
			gluPerspective(80, ((float)width)/((float)height), 0.1, 1000);
			glMatrixMode(GL_MODELVIEW_MATRIX);

			glEnable(GL_DEPTH_TEST);
		}

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		m_Player = new Player();
		m_Cam = new Camera(m_Player);
	}

	void ShrimpWindow::setFPSLimit(int fps) {
		m_Window->setFramerateLimit(fps);
	}

	void ShrimpWindow::update() {
		glPopMatrix();
		m_Window->display();
	}

	void ShrimpWindow::poll() {

		sf::Event event;

		while (m_Window->pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				m_Window->close();
			}
			else if (event.type == sf::Event::Resized)
			{
				glViewport(0, 0, event.size.width, event.size.height);
			}
			else if (event.type == sf::Event::LostFocus) {
				m_Focus = false;
			}
			else if (event.type == sf::Event::GainedFocus) {
				m_Focus = true;
			}
		}

		m_Cam->useCam();
		m_Player->tick();
	}

	void ShrimpWindow::clear() {
		if (m_Type == WINDOW_GL_3D) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
		else {
			glClear(GL_COLOR_BUFFER_BIT);
		}
		
		m_MousePos = sf::Vector2i(sf::Mouse::getPosition(*m_Window).x, m_Window->getSize().y - (sf::Mouse::getPosition(*m_Window).y));

		glPushMatrix();
	}

	bool ShrimpWindow::isCloseRequested() {
		return !m_Window->isOpen();
	}

	sf::Vector2i ShrimpWindow::getSize() {
		return *(new sf::Vector2i(m_Width,m_Height));
	}
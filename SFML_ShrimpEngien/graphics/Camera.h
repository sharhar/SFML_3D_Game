#pragma once

#include <SFML/OpenGL.hpp>
#include "../objects/Player.h"

class Camera {
private:
	Player* m_Player;
public:
	Camera(Player* p);

	void useCam();
};
#pragma once

#define _USE_MATH_DEFINES

#include <math.h>

class Math {
public:
	static float toRadians(float deg) {
		return (deg / 180) * M_PI;
	}
};
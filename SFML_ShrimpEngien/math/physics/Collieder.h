#pragma once

#include "../../graphics/shapes/Cube.h"

class Collieder {
public:
	static bool boxCollision(Cube* c1, Cube* c2) {
		sf::Vector3f cube1Pos = sf::Vector3f(c1->pos->x - c1->size->x / 2, c1->pos->y - c1->size->y / 2, c1->pos->z - c1->size->z / 2);
		sf::Vector3f cube2Pos = sf::Vector3f(c2->pos->x - c2->size->x / 2, c2->pos->y - c2->size->y / 2, c2->pos->z - c2->size->z / 2);

		if (cube1Pos.y + c1->size->y < cube2Pos.y) {
			return false;
		}

		if (cube2Pos.y + c2->size->y < cube1Pos.y) {
			return false;
		}

		if (cube1Pos.x + c1->size->x < cube2Pos.x) {
			return false;
		}

		if (cube2Pos.x + c2->size->x < cube1Pos.x) {
			return false;
		}

		if (cube1Pos.z + c1->size->z < cube2Pos.z) {
			return false;
		}

		if (cube2Pos.z + c2->size->z < cube1Pos.z) {
			return false;
		}

		return true;
	}
};
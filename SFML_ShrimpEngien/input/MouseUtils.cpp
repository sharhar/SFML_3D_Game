#include "MouseUtils.h"

MouseUtils::MouseUtils(ShrimpWindow* w) {
	sensitivity = 1;
	cen = true;
	win = w;

	sf::Mouse::setPosition(sf::Vector2i(w->getSize().x / 2, w->getSize().y / 2), *w->getWindow());

	lastPos = sf::Vector2i(w->getSize().x / 2, w->getSize().y / 2);
}

void MouseUtils::tick() {
	sf::Vector2i pos = win->getMousePos();

	if (pos.x > lastPos.x) {
		right(pos.x - lastPos.x);
	} else if (pos.x < lastPos.x) {
		left(lastPos.x - pos.x);
	}

	if (pos.y > lastPos.y) {
		up(pos.y - lastPos.y);
	} else if (pos.y < lastPos.y) {
		down(lastPos.y - pos.y);
	}

	lastPos = win->getMousePos();

	if (cen && win->getFocus()) {
		center(pos);
	}
}

void MouseUtils::center(sf::Vector2i pos) {
	if (pos.y < 100) {
		sf::Mouse::setPosition(sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2), *win->getWindow());
		lastPos = sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2);
	}
	else if (pos.y > win->getSize().y-100) {
		sf::Mouse::setPosition(sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2), *win->getWindow());
		lastPos = sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2);
	}

	if (pos.x < 100) {
		sf::Mouse::setPosition(sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2), *win->getWindow());
		lastPos = sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2);
	}
	else if (pos.x > win->getSize().x - 100) {
		sf::Mouse::setPosition(sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2), *win->getWindow());
		lastPos = sf::Vector2i(win->getSize().x / 2, win->getSize().y / 2);
	}
}

void MouseUtils::up(int amount) {
	win->getPlayer()->getRot()->x -= (amount/4)*sensitivity;

	if (win->getPlayer()->getRot()->x < -90) {
		win->getPlayer()->getRot()->x = -90;
	}
}

void MouseUtils::down(int amount) {
	win->getPlayer()->getRot()->x += (amount / 4)*sensitivity;

	if (win->getPlayer()->getRot()->x > 90) {
		win->getPlayer()->getRot()->x = 90;
	}
}

void MouseUtils::left(int amount) {
	win->getPlayer()->getRot()->y -= (amount / 4)*sensitivity;
}

void MouseUtils::right(int amount) {
	win->getPlayer()->getRot()->y += (amount / 4)*sensitivity;
}
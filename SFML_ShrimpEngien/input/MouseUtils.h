#pragma once

#include <SFML/System.hpp>
#include "../graphics/Window.h"

#include <iostream>

using namespace std;

class MouseUtils {
public:
	ShrimpWindow* win;
	bool cen;
	int sensitivity;

	sf::Vector2i lastPos;

	MouseUtils(ShrimpWindow* w);

	void center(sf::Vector2i pos);
	void tick();

	void up(int amount);
	void down(int amount);
	void left(int amount);
	void right(int amount);

	sf::Vector2i convert(sf::Vector2i pos) {
		return sf::Vector2i(pos.x,win->getMousePos().y - pos.y);
	}
};
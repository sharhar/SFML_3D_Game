#pragma once

#include "../graphics/Window.h"

class UIObject {
private:
	sf::RectangleShape* m_Shape;
	sf::Texture* m_Texture;
	ShrimpWindow* m_Win;
public:
	UIObject(sf::RectangleShape* m_Shape, sf::Texture* m_Texture, ShrimpWindow* m_Win);

	void setPosition(int x, int y) {
		m_Shape->setPosition(sf::Vector2f(x,y));
	}

	sf::RectangleShape* getShape() {
		return m_Shape;
	}
};
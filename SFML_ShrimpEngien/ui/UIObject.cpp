#include "UIObject.h"

UIObject::UIObject(sf::RectangleShape* shape, sf::Texture* texture, ShrimpWindow* win) {
	m_Shape = shape;
	m_Texture = texture;
	m_Win = win;
}
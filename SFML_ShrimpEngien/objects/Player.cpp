#include "Player.h"

Player::Player() {
	m_Pos = new sf::Vector3f();
	m_Rot = new sf::Vector3f();
	m_HitBox = new Cube(m_Pos,new sf::Vector3f(0.5f,2,0.5f),new sf::Vector3f(), NULL);

	other = nullptr;
	setMoveType(MOVE_TYPE_FREE_ROME);
	yVel = 0;
}

void Player::tick() {
	if (m_MoveType == MOVE_TYPE_FPS) {
		yVel -= gForce;
		moveYCol(yVel);
	}
}

void Player::moveX(float amount) {
	moveZCol((float)(amount * sin(Math::toRadians(m_Rot->y))));
	moveXCol((float)(amount * cos(Math::toRadians(m_Rot->y))));
}

void Player::moveZ(float amount) {
	if (m_MoveType == MOVE_TYPE_FREE_ROME) {
		moveZCol((float)(amount * sin(Math::toRadians(m_Rot->y + 90)) * cos(Math::toRadians(m_Rot->x))));
		moveXCol((float)(amount * cos(Math::toRadians(m_Rot->y + 90)) * cos(Math::toRadians(m_Rot->x))));
		moveYCol((float)(amount * -cos(Math::toRadians(m_Rot->x + 90)) * sin(Math::toRadians(m_Rot->z + 90))));
	}
	else if (m_MoveType == MOVE_TYPE_FPS){
		moveZCol((float)(amount * sin(Math::toRadians(m_Rot->y + 90))));
		moveXCol((float)(amount * cos(Math::toRadians(m_Rot->y + 90))));
	}
}

void Player::moveXCol(float amount) {
	m_Pos->x += amount;
	if (Collieder::boxCollision(m_HitBox,other)) {
		m_Pos->x -= amount;
	}
}

void Player::moveYCol(float amount) {
	m_Pos->y += amount;
	if (Collieder::boxCollision(m_HitBox, other)) {
		yVel = 0;
		m_Pos->y -= amount;
	}
}

void Player::moveZCol(float amount) {
	m_Pos->z += amount;
	if (Collieder::boxCollision(m_HitBox, other)) {
		m_Pos->z -= amount;
	}
}
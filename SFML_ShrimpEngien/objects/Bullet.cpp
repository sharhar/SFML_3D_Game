#include "Bullet.h"

Bullet::Bullet(sf::Vector3f pos, sf::Vector3f slope, sf::Texture* tex) {
	m_Pos = pos;
	m_Slope = slope;

	m_Cube = new Cube(&m_Pos,new sf::Vector3f(1,1,1),new sf::Vector3f(),tex);
}

void Bullet::update() {
	//m_Pos.x += m_Slope.x;
	//m_Pos.y += m_Slope.y;
	//m_Pos.z += m_Slope.z;
}

void Bullet::render() {
	m_Cube->render();
}
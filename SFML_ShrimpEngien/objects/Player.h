#pragma once

#include <SFML/System.hpp>
#include "../math/Math.h"
#include "../graphics/shapes/Cube.h"
#include "../math/physics/Collieder.h"

class Player {
private:
	sf::Vector3f* m_Pos;
	sf::Vector3f* m_Rot;
	Cube* m_HitBox;

	Cube* other;
	int m_MoveType;
	float yVel;
	const float gForce = 0.01f;
public:
	static const int MOVE_TYPE_FREE_ROME = 1;
	static const int MOVE_TYPE_FPS = 2;
	
	Player();

	void tick();

	void moveX(float amount);
	void moveZ(float amount);

	sf::Vector3f* getPos() {
		return m_Pos;
	}

	sf::Vector3f* getRot() {
		return m_Rot;
	}

	Cube* getHitBox() {
		return m_HitBox;
	}

	void setOther(Cube* o) {
		other = o;
	}

	void setMoveType(int type) {
		m_MoveType = type;
	}

	void setYVel(float amount) {
		yVel = amount;
	}
private:
	void moveXCol(float amount);
	void moveYCol(float amount);
	void moveZCol(float amount);
};
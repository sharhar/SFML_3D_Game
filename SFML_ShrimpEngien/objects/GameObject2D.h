#pragma once

#include <SFML/Graphics.hpp>
#include "../graphics/Renderer.h"
#include "../graphics/Window.h"

class GameObject2D{
public:
	sf::Texture m_Tex;
	sf::Vector2f m_Pos;
	sf::Vector2f m_Size;
	float m_Rotation;
	bool m_AbleToGetOutOfScreen;
	int ID = 0;

	ShrimpWindow *m_Window = NULL;
public:
	GameObject2D(float x, float y, float w, float h, float r, sf::Texture tex);
	void setAbleToGetOutOfScreen(bool b);
	void setWindow(ShrimpWindow* win);
	void render();
	void moveX(float amount);
	void moveY(float amount);
	void rotate(float amount);
	bool collidingWithObject(GameObject2D *ob);
	
};
#include "GameObject2D.h"

GameObject2D::GameObject2D(float x, float y, float w, float h, float r, sf::Texture tex) {
	m_Pos = *(new sf::Vector2f(x,y));
	m_Size = *(new sf::Vector2f(w, h));
	m_Tex = tex;
	m_Rotation = r;
}

void GameObject2D::render() {
	Renderer::renderRect2DTex(m_Pos.x,m_Pos.y,m_Size.x,m_Size.y,m_Rotation,&m_Tex);
}

void GameObject2D::setWindow(ShrimpWindow* win) {
	m_Window = win;
}

void GameObject2D::moveX(float amount) {
	if (m_Window != NULL && !m_AbleToGetOutOfScreen) {
		if (m_Pos.x + amount < 0 || m_Pos.x + amount + m_Size.x > m_Window->getSize().x) {
			return;
		}
	}
	
	m_Pos.x += amount;
}

void GameObject2D::moveY(float amount) {
	if (m_Window != NULL && !m_AbleToGetOutOfScreen) {
		if (m_Pos.y + amount < 0 || m_Pos.y + amount + m_Size.y > m_Window->getSize().y) {
			return;
		}
	}

	m_Pos.y += amount;
}

void GameObject2D::setAbleToGetOutOfScreen(bool b) {
	m_AbleToGetOutOfScreen = b;
}

void GameObject2D::rotate(float amount) {
	m_Rotation += amount;
}

bool GameObject2D::collidingWithObject(GameObject2D *ob) {
	if (m_Pos.x + m_Size.x < ob->m_Pos.x) {
		return false;
	}

	if (m_Pos.x > ob->m_Pos.x + ob->m_Size.x) {
		return false;
	}

	if (m_Pos.y + m_Size.y < ob->m_Pos.y) {
		return false;
	}

	if (m_Pos.y > ob->m_Pos.y + ob->m_Size.y) {
		return false;
	}

	return true;
}
#pragma once

#include "../graphics/shapes/Cube.h"

class Bullet {
private:
	sf::Vector3f m_Pos;
	sf::Vector3f m_Slope;

	Cube* m_Cube;
public:
	Bullet(sf::Vector3f pos, sf::Vector3f slope,sf::Texture* tex);

	void update();
	void render();
};
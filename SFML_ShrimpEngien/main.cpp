#include "graphics/Window.h"
#include "graphics/Renderer.h"
#include "graphics/shapes/Cube.h"
#include "input/MouseUtils.h"
#include "ui/UIObject.h"
#include "utils/ShotHandeler.h"

#include <iostream>
#include <string>

using namespace std;

//optimization, vectors<?> (c++), inheritance

int main() {

	ShrimpWindow window(1600, 900, "SFML Engien", ShrimpWindow::WINDOW_GL_3D);
	window.showMouse(false);
	window.getPlayer()->setMoveType(Player::MOVE_TYPE_FPS);
	MouseUtils m(&window);

	glClearColor(0,0,1,1);

	ShotHandler shot = ShotHandler(&window);

	sf::Texture tex;
	tex.loadFromFile("res/Brick.png");

	Cube floor(new sf::Vector3f(0, -10, 0), new sf::Vector3f(20, 5, 20), new sf::Vector3f(0, 0, 0), &tex);
	sf::CircleShape shape(50);

	UIObject* ob = new UIObject(new sf::RectangleShape(sf::Vector2f(10, 10)), &tex, &window);
	ob->setPosition((1600/2) - 5,(900/2) - 5);

	window.getPlayer()->setOther(&floor);
	window.setFPSLimit(60);
	while (!window.isCloseRequested()) {
		window.clear();
		window.poll();

		m.tick();

		float speed = 0.2f;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			window.getWindow()->close();
			break;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
			window.getPlayer()->moveZ(-speed);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
			window.getPlayer()->moveZ(speed);
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
			window.getPlayer()->moveX(-speed);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			window.getPlayer()->moveX(speed);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
			window.getPlayer()->setYVel(speed);
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			shot.shoot();
		}

		shot.update();
		
		floor.render();
		
		
		window.getWindow()->pushGLStates();
		//window.getWindow()->draw(*ob->getShape());
		window.getWindow()->popGLStates();

		shot.render();

		window.update();
	}

	return 0;
}

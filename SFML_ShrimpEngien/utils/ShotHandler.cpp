#include "ShotHandeler.h"

ShotHandler::ShotHandler(ShrimpWindow* window) {
	m_Shots = vector<Bullet>();
	m_BulletSpeed = 1;
	m_Window = window;

	m_BulletTexture = sf::Texture();
	m_BulletTexture.create(1,1);

	sf::Image black = sf::Image();
	black.create(1,1);
	black.createMaskFromColor(sf::Color::Black);

	m_BulletTexture.update(black);

	sf::Texture tex;
	tex.loadFromFile("res/Brick.png");

	m_BulletTexture = tex;
}

void ShotHandler::shoot() {
	sf::Vector3f pos = sf::Vector3f();
	sf::Vector3f slope = sf::Vector3f();

	pos = *m_Window->getPlayer()->getPos();

	slope.z = -((float)(m_BulletSpeed * sin(Math::toRadians(m_Window->getPlayer()->getRot()->y + 90)) * cos(Math::toRadians(m_Window->getPlayer()->getRot()->x))));
	slope.x = -((float)(m_BulletSpeed * cos(Math::toRadians(m_Window->getPlayer()->getRot()->y + 90)) * cos(Math::toRadians(m_Window->getPlayer()->getRot()->x))));
	slope.y = -((float)(m_BulletSpeed * -cos(Math::toRadians(m_Window->getPlayer()->getRot()->x + 90)) * sin(Math::toRadians(m_Window->getPlayer()->getRot()->z + 90))));

	Bullet bullet = Bullet(pos,slope,&m_BulletTexture);

	m_Shots.push_back(bullet);
}

void ShotHandler::render() {
	for (Bullet b:m_Shots) {
		b.render();
	}
}

void ShotHandler::update() {
	for (Bullet b : m_Shots) {
		b.update();
	}
}
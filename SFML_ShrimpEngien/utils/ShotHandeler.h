#pragma once

#include "../objects/Bullet.h"
#include <vector>
#include "../math/Math.h"
#include "../graphics/Window.h"
#include <iostream>

using namespace std;

class ShotHandler {
private:
	float m_BulletSpeed;
	vector<Bullet> m_Shots;
	ShrimpWindow* m_Window;
	sf::Texture m_BulletTexture;
public:
	ShotHandler(ShrimpWindow* window);

	void shoot();
	void render();
	void update();
};